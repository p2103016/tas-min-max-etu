# Version minimale de CMake requise
cmake_minimum_required(VERSION 3.12)

project(TasMinMax)

# Sources
file(GLOB SRC src/*.cpp src/*.hpp)


# Applications
add_executable(test_tas ${SRC})
